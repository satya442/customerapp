const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;


const users = new Schema({
    fullName: { type: String, required: true },
    emailId: { type: String, unique: true, required: true },
    password: { type: Buffer, required: true },
}, {
    timestamps: true
});

let Users = mongoose.model('users', users);


const authtokens = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'users' },
    token: { type: Buffer },
    tokenExpiryTime: { type: Date }
}, {
    timestamps: true
});
let Authtokens = mongoose.model('authtokens', authtokens);


module.exports = {
    Users, Authtokens
}