module.exports = (app, express) => {

    const router = express.Router();
    const exportLib = require('../../services/Exports');
    const UserController = require('./Controller');
    const Validators = require('./Validator');

    router.post('/register', Validators.registerValidator(), Validators.validate, (req, res, next) => {
        const userObj = (new UserController()).boot(req, res);
        return userObj.register();
    });

    router.post('/login', Validators.loginValidator(), Validators.validate, (req, res, next) => {
        const userObj = (new UserController()).boot(req, res);
        return userObj.login();
    });

    app.use(exportLib.configs.baseApiUrl, router);
}