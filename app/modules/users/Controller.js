const BaseController = require('../Base/Controller');
const Model = require("../Base/Model");
const Users = require('./Model').Users;
const exportLib = require('../../services/Exports');


class UsersController extends BaseController {
    constructor() {
        super();
    }

    /********************************************************
        Purpose: User Registration
        Method: Post
        Parameter:
        {
            "emailId":"satya@grr.la",
            "fullName":"lakshmi matta",
            "password":"satya",
        }                  
        Return: JSON String
    ********************************************************/
    async register() {
        try {
            let data = this.req.body;
            data.emailId = data.emailId.toLowerCase();
            let filter = { emailId: data.emailId }
            /******* check emailId is exist or not ********/
            const user = await Users.findOne(filter);
            /****** if user exist give error ********/
            if (!exportLib._.isEmpty(user) && user.emailId) {
                return this.res.send({ status: 0, message: exportLib.i18n.__("USER_ALREADY_EXISTS_WITH_ABOVE_EMAILID") });
            } else {
                let password = await exportLib.commonObj.ecryptPassword({ password: data['password'] });
                data.password = password;
                /****** save new user ********/
                const newUser = await new Model(Users).store(data);
                if (exportLib._.isEmpty(newUser)) {
                    return this.res.send({ status: 0, message: exportLib.i18n.__('USER_NOT_SAVED') })
                }
                return this.res.send({ status: 1, message: exportLib.i18n.__('REGISTERED_SUCCESSFULLY') });
            }
        } catch (error) {
            console.log("error", error)
            return this.res.send({ status: 0, message: error });
        }

    }

    /********************************************************
    Purpose: User Login
    Method: Post
    Parameter:
    {
        "emailId":"satya@grr.la",
        "password":"satya@442"
    }              
    Return: JSON String
    ********************************************************/
    async login() {
        let _this = this;
        try {
            let data = _this.req.body;
            const user = await Users.findOne({ emailId: data.emailId.toLowerCase() });
            if (exportLib._.isEmpty(user)) {
                return _this.res.send({ status: 0, message: exportLib.i18n.__("USER_NOT_FOUND") });
            }
            else {
                const status = await exportLib.commonObj.verifyPassword({ password: data.password, savedPassword: user.password });
                if (!status) {
                    return _this.res.send({ status: 0, message: exportLib.i18n.__("INVALID_CREDENTIALS_AUTHENTICATION_FAILED") });
                }
            }
            delete data.password;
            let updatedUser = await Users.findByIdAndUpdate(user._id, data).select('_id fullName emailId').lean();
            let token = await exportLib.globalObj.getToken({ id: user._id });

            updatedUser.token = token;
            return _this.res.send({ status: 1, message: exportLib.i18n.__("LOGIN_SUCCESSFULLY"), data: updatedUser });
        } catch (error) {
            console.log("error", error)
            return _this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') });
        }
    }

}
module.exports = UsersController;

