const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;


const products = new Schema({
    productName: { type: String, required: true },
    price: { type: Number, required: true },
    SKU: { type: String, required: true },
    customUrl: { type: String, required: true },
    description: { type: String, required: true },
    productImage: { type: String, required: true },
    images: [{ type: String }],
}, {
    timestamps: true
});

let Products = mongoose.model('products', products);

module.exports = {
    Products
}