const BaseController = require('../Base/Controller');
const Model = require('../Base/Model');
const Products = require('./Model').Products;
const exportLib = require("../../services/Exports");
const json2csv = require('json2csv').parse;
const csv2json = require("csvjson-csv2json");
const configs = require('../../../configs/configs');
const File = require("../../services/File");
const Form = require("../../services/Form");



class ProductsController extends BaseController {
    constructor() {
        super();
    }

    /********************************************************
   Purpose: download sample csv for products bulkUpload
   Method: get
   Authorisation: true
   ********************************************************/
    async downloadSampleCsv() {
        let _this = this;
        try {
            let project = ['productName', 'price', 'salePrice', 'SKU', 'description'];
            let tempData = [{
                productName: "boAt Rockerz 450 Bluetooth Headset", price: 1499, SKU: "sku-boa-1", description: "These boAt Headphones are not only stylish but they also ensure that you get to listen to your favourite music comfortably. They feature adaptive and adjustable ear cups that provide comfortable usage. With 40 mm drivers, you can enjoy an immersive audio experience so that you can feel every beat of your favourite songs. Thanks to the 800 mAh battery, you can enjoy up to 8 hours of music playback as they provide non-stop listening without suddenly running out of battery power.", images: "image1.jpg,image2.jpg", productImage: "image.jpg"
            }]
            // code for csv download
            const opts = { project };

            const csv = json2csv(tempData, opts);
            const filePathAndName = "sampleProducts" + Date.now() + ".csv";
            const filePath = exportLib.path.join(__dirname, "../../../", "public/csv/", filePathAndName);
            let data1 = { filePath: configs.apiUrl + '/public/csv/' + filePathAndName }
            exportLib.fs.writeFile(filePath, csv, function (err) {
                if (err) return _this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') });
                else return _this.res.send({ status: 1, message: exportLib.i18n.__('CSV_DOWNLOAD'), data: data1 });
            })

        } catch (error) {
            console.log("error", error)
            return _this.res.send({ status: 0, message: error });
        }
    }

    /********************************************************
      Purpose: Products bulk upload
      Method: Post
      Authorisation: true
      Form-Data:file(choose the csv file with products details)
      ********************************************************/
    async bulkUploadProduct() {
        let _this = this;
        try {
            let form = new Form(_this.req);
            let formObject = await form.parse();
            if (exportLib._.isEmpty(formObject.files))
                return _this.res.send({ status: 0, message: exportLib.i18n.__('PLEASE_SEND_FILE') });

            const file = new File(formObject.files);
            let data = await file.readFile(formObject.files.file[0].path);
            let options = {
                delimiter: ',', // optional
                quote: '"', // optional
                parseNumbers: true
            }
            let extProdArr = []; let jsonData = []
            let insertProdArr = []; let requiredArr = []; let count = 1; let notFoundArr = [];
            jsonData = csv2json(data, options);
            /***** removing duplications from the coming data *****/
            jsonData = await jsonData.filter((ele, ind) => ind === jsonData.findIndex(elem => elem.SKU === ele.SKU))
            /**** condition to find required fields ******/
            for (let s = 0; s < jsonData.length; s++) {
                if (!jsonData[s].productName || !jsonData[s].description || (jsonData[s].price == '' || jsonData[s].price == 0) || !jsonData[s].SKU || !jsonData[s].productImage) {
                    requiredArr.push(s + 2)
                }
            }
            if (!exportLib._.isEmpty(requiredArr))
                return _this.res.send({ status: 0, message: exportLib.i18n.__('REQUIRED'), data: requiredArr });

            for (let x = 0; x < jsonData.length; x++) {
                let imageArr = []
                if (jsonData[x].images) {
                    imageArr = jsonData[x].images.split(',')
                }
                let requestData = {
                    productName: jsonData[x].productName,
                    SKU: jsonData[x].SKU.toString(),
                    customUrl: jsonData[x].productName.replace(/ /g, '-').replace(/[^\w-]+/g, '').replace(/\-\-+/g, '-') + x,
                    price: parseInt(jsonData[x].price),
                    description: jsonData[x].description,
                    productImage: jsonData[x].productImage,
                    images: imageArr
                }

                let existProductSKU = await Products.findOne({ "SKU": requestData.SKU });
                if (exportLib._.isEmpty(existProductSKU)) { insertProdArr.push(requestData) }
                else { extProdArr.push(requestData) }
                count = count + 1
            }
            if (!exportLib._.isEmpty(notFoundArr)) { return _this.res.send({ status: 0, message: exportLib.i18n.__('NOT_FOUND_DETAILS_OF_PRODUCT'), data: notFoundArr }); }
            if (extProdArr.length > 0) {
                if (insertProdArr.length > 0) {
                    await new Model(Products).bulkInsert(insertProdArr)
                }
                let proDetails = extProdArr
                for (let x = 0; x < proDetails.length; x++) {
                    let details = await Products.findOne({ SKU: proDetails[x].SKU });
                    let requestData = {
                        productName: proDetails[x].productName,
                        SKU: proDetails[x].SKU,
                        customUrl: proDetails[x].customUrl,
                        price: proDetails[x].price,
                        description: proDetails[x].description,
                        productImage: proDetails[x].productImage,
                        images: proDetails[x].images,
                    }
                    if (exportLib._.isEmpty(details)) {
                        await new Model(Products).store(requestData);
                    }
                    else {
                        await Products.findOneAndUpdate({ "SKU": requestData.SKU }, requestData);
                    }
                }
                return _this.res.send({ status: 1, message: exportLib.i18n.__('UPDATED_SUCCESSFULLY') });
            }
            else {
                await new Model(Products).bulkInsert(insertProdArr)
                return _this.res.send({ status: 1, message: exportLib.i18n.__('ADDED_SUCCESSFULLY') });
            }
        } catch (error) {
            console.log("error", error)
            return _this.res.send({ status: 0, message: error });
        }
    }

    /********************************************************
   Purpose: UploadBulkMultipleImages For Uploading Images Of Product (Here all the images are converting into .jpg format)
   Method: Post
   Authorisation: true
   Parameter:
   form-data
   file: send multiple files
   Return: JSON String
   ********************************************************/
    async uploadBulkMultipleImages() {
        let _this = this;
        try {
            let form = new Form(_this.req);
            let formObject = await form.parse();
            if (exportLib._.isEmpty(formObject.files))
                return _this.res.send({ status: 0, message: exportLib.i18n.__('PLEASE_SEND_FILE') });
            let filesData = new File(formObject.files);
            var count = formObject.files.file.length;
            for (let i = 0; i < count; i++) {
                let files = filesData.file.file[i];
                let fileName = files.originalFilename.split(".");
                let imageName = fileName[0] + ".jpg";
                await filesData.saveImage({ filePath: files.path, imageName });
            }
            return _this.res.send({ status: 1, message: exportLib.i18n.__('FILE_UPLOAD') });
        }
        catch (error) {
            console.log("error- ", error);
            _this.res.send({ status: 0, message: error });
        }
    }

    /********************************************************
    Purpose: products listing
    Method: Post
    Authorisation: false
    Parameter:
    {
        "page":1,
        "pagesize":10
    }
    Return: JSON String
    ********************************************************/
    async productsListing() {
        let _this = this;
        try {
            let data = _this.req.body;
            /****** pagination details starts *******/
            let page = (data.page) ? parseInt(data.page) : 1;
            let pagesize = (data.pagesize) ? parseInt(data.pagesize) : 999;
            let skip = (page && pagesize) ? ((page - 1) * pagesize) : 0;
            let limit = pagesize ? pagesize : 20;
            let sort = data.sort ? data.sort : { _id: -1 };
            /****** pagination details ends *******/
            let products = await Products.find({}, { __v: 0, createdAt: 0, updatedAt: 0 }).sort(sort).skip(skip).limit(limit);
            let total = await Products.find({}).countDocuments();
            /***** final output of the products *****/
            let resData = {
                currentPage: page,
                total,
                products,
            };
            return _this.res.send({ status: 1, message: exportLib.i18n.__('DETAILS'), data: resData });
        } catch (error) {
            console.log("error", error);
            return _this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') });
        }
    }

}
module.exports = ProductsController
