module.exports = (app, express) => {

    const router = express.Router();
    const exportLib = require('../../services/Exports');
    const ProductsController = require('./Controller');

    router.get('/downloadSampleCsv', exportLib.Globals.isAuthorised, (req, res, next) => {
        const productObj = (new ProductsController()).boot(req, res);
        return productObj.downloadSampleCsv();
    });

    router.post('/bulkUploadProduct', exportLib.Globals.isAuthorised, (req, res, next) => {
        const productObj = (new ProductsController()).boot(req, res);
        return productObj.bulkUploadProduct();
    });

    router.post('/uploadBulkMultipleImages', exportLib.Globals.isAuthorised, (req, res, next) => {
        const productObj = (new ProductsController()).boot(req, res);
        return productObj.uploadBulkMultipleImages();
    });

    router.post('/productsListing', exportLib.Globals.isAuthorised, (req, res, next) => {
        const productObj = (new ProductsController()).boot(req, res);
        return productObj.productsListing();
    });

    app.use(exportLib.configs.baseApiUrl, router);
}