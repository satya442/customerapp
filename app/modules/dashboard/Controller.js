const BaseController = require('../Base/Controller');
const Orders = require('../order/Model').Orders;
const exportLib = require('../../services/Exports');

class DashboardController extends BaseController {
    constructor() {
        super();
    }

    /********************************************************
      Purpose: getting top 5 best customers by purchased amount
      Method: Get
      Authorisation: true
      Return: JSON String
      ********************************************************/
    async getBestCustomers() {
        let _this = this;
        try {
            let customers = await Orders.aggregate([
                { $match: { orderStatus: { $ne: "cancelled" } } },
                {
                    $group: {
                        _id: "$userId",
                        userId: { $first: "$userId" },
                        totalAmount: { $sum: "$totalAmount" }
                    }
                },
                { $sort: { totalAmount: -1 } },
                { $lookup: { from: "users", localField: "userId", foreignField: "_id", as: "users" } },
                { $unwind: { path: "$users", includeArrayIndex: "arrayIndex", preserveNullAndEmptyArrays: true } },
                { $limit: 5 },
                { $project: { userName: "$users.fullName", emailId: "$users.emailId", totalAmount: 1 } }
            ]);
            return _this.res.send({ status: 1, message: exportLib.i18n.__('DETAILS'), data: customers })
        } catch (error) {
            console.log("error", error)
            return _this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') })
        }
    }

    /********************************************************
    Purpose: Getting statistics of order.
    Method: Get
    Authorisation: true
    Return: JSON String
    ********************************************************/
    async statisticsOfOrdersCount() {
        try {
            const orderStat = await Orders.aggregate([
                { $match: { orderStatus: { $ne: 'cancelled' } } },
                {
                    $group: {
                        _id: {
                            year: { $year: "$createdAt" }, month: { $month: "$createdAt" }, day: { $dayOfMonth: "$createdAt" }
                        },
                        count: { $sum: 1 },
                        createdAt: { $first: "$createdAt" }
                    }
                },
                { $sort: { createdAt: 1 } }
            ]);
            return this.res.send({ status: 1, message: exportLib.i18n.__('DETAILS'), data: orderStat })
        } catch (error) {
            console.log("error", error)
            return this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') })
        }
    }

}
module.exports = DashboardController;
