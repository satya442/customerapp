module.exports = (app, express) => {

    const router = express.Router();
    const exportLib = require('../../services/Exports');
    const DashboardController = require('./Controller');

    router.get('/getBestCustomers', exportLib.Globals.isAuthorised, (req, res, next) => {
        const dashObj = (new DashboardController()).boot(req, res);
        return dashObj.getBestCustomers();
    });

    router.get('/statisticsOfOrdersCount', exportLib.Globals.isAuthorised, (req, res, next) => {
        const dashObj = (new DashboardController()).boot(req, res);
        return dashObj.statisticsOfOrdersCount();
    });


    app.use(exportLib.configs.baseApiUrl, router);
}
