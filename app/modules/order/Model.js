const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;


const transport = new Schema({
    fullName: { type: String, trim: true },
    address: { type: String, trim: true },
    city: { type: String, trim: true },
    state: { type: String, trim: true },
    zipCode: { type: String, trim: true },
    mobileNo: { type: String, trim: true },
}, {
    _id: false
})

const product = new Schema({
    productName: { type: String },
    productImage: { type: String },
    SKU: { type: String },
    customUrl: { type: String },
    quantity: { type: Number },
    price: { type: Number },
    salePrice: { type: Number },
}, {
    _id: false
})

const orders = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'users' },
    shippingAddress: transport,
    billingAddress: transport,
    products: [product],
    count: { type: Number },
    totalAmount: { type: Number },
    orderStatus: { type: String, enum: ['pending', 'cancelled'], default: "pending" },
    orderId: { type: String },
}, {
    timestamps: true
});

let Orders = mongoose.model('orders', orders);
module.exports = {
    Orders
}
