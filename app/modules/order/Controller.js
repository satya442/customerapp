const BaseController = require('../Base/Controller');
const Model = require("../Base/Model");
const exportLib = require('../../services/Exports');
const Users = require('../users/Model').Users;
const Orders = require('./Model').Orders;
const Products = require('../products/Model').Products;


class OrderController extends BaseController {
    constructor() {
        super();
    }

    /********************************************************
        Purpose: for placing order from front-side
        Method: Post
        Authorisation: true
        Parameter:
        {
            "shippingAddress":{
                "fullName":"lakshmi",
                "address":"near govt hospital,seethanagaram",
                "city":"rajahmundry",
                "state":"andhra pradesh",
                "zipCode":"533287",
                "mobileNo":"7207334583"
            },
            "products":[
                {
                    "productId":"5f75e507bb834236dc356b05",
                    "quantity":4
                }
            ]
        }
        Return: JSON String
        ********************************************************/
    async checkOut() {
        let _this = this;
        try {
            let data = _this.req.body;
            let user = await Users.findOne({ _id: _this.req.currentUser._id }, { fullName: 1, emailId: 1 });
            if (exportLib._.isEmpty(user)) {
                return _this.res.send({ status: 0, message: exportLib.i18n.__('NOT_FOUND') });
            }
            /***** adding address of user to ship the order *****/
            data['billingAddress'] = data.shippingAddress;
            data.userId = user._id;
            /***** creating orderId ******/
            let ord = await Orders.find();
            data.orderId = "ord" + "-" + (ord.length + 1);
            let productsArray = data.products;
            let productDetails = await this.productDetails({ productsArray })
            let value = { ...data, ...productDetails }
            const newOrder = await new Model(Orders).store(value);
            if (exportLib._.isEmpty(newOrder)) { _this.res.send({ status: 0, message: exportLib.i18n.__('NOT_SAVED') }); }
            return _this.res.send({ status: 1, message: exportLib.i18n.__('ORDER_PLACED') });
        } catch (error) {
            console.log("error", error)
            return _this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') });
        }
    }

    /********************************************************
       Purpose: for cancel order  from front side
       Method: Post
       Authorisation: true
       Parameter:
       {
           "orderId":"5e203855aee64818ab03f141"
       }
       Return: JSON String
       ********************************************************/
    async cancelOrder() {
        let _this = this;
        try {
            /***** finding user details ******/
            let user = await Users.findOne({ _id: _this.req.currentUser._id }, { fullName: 1, emailId: 1 }).lean();
            if (exportLib._.isEmpty(user)) {
                return _this.res.send({ status: 0, message: exportLib.i18n.__('NOT_FOUND') });
            }
            /***** finding order details ******/
            let order = await Orders.findOneAndUpdate({ _id: _this.req.body.orderId, userId: user._id }, { orderStatus: 'cancelled' }, { upsert: true });
            if (exportLib._.isEmpty(order)) {
                return _this.res.send({ status: 0, message: exportLib.i18n.__('NOT_FOUND') });
            }
            return this.res.send({ status: 1, message: exportLib.i18n.__('ORDERED_CANCELLED') });
        } catch (error) {
            console.log("error", error)
            return _this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') });
        }
    }

    /********************************************************
       Purpose: Get order details
       Method: get
       Return: JSON String
       ********************************************************/
    async getOrderDetails() {
        try {
            let order = await Orders.findOne({ _id: this.req.params.orderId }, { __v: 0, updatedAt: 0 });
            if (exportLib._.isEmpty(order)) {
                return this.res.send({ status: 0, message: exportLib.i18n.__('NOT_FOUND') });
            }
            return this.res.send({ status: 1, message: exportLib.i18n.__("DETAILS"), data: order });
        } catch (error) {
            console.log("error", error)
            return this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') });
        }
    }

    /********************************************************
     Purpose: update order details
     Method: Post
     Authorisation: true
     Parameter:
     {
         "products":[
              {
                  "productId":"5f75e507bb834236dc356b05",
                  "quantity":4
              }
          ],
          "orderId":"5e203855aee64818ab03f141"
     }
     Return: JSON String
     ********************************************************/
    async updateOrder() {
        try {
            let data = this.req.body;
            /***** finding order details ******/
            let order = await Orders.findOne({ _id: data.orderId }, { _id: 1, orderStatus: 1 }).lean();
            if (exportLib._.isEmpty(order)) {
                return this.res.send({ status: 0, message: exportLib.i18n.__('NOT_FOUND') });
            }
            if (order && order.orderStatus && order.orderStatus == "cancelled") {
                return this.res.send({ status: 0, message: exportLib.i18n.__('NOT_POSSIBLE_TO_UPDATE') });
            }
            let productsArray = data.products;
            if (productsArray.length > 0) {
                let productDetails = await this.productDetails({ productsArray });
                /***** finding order details ******/
                let orderDetails = await Orders.findOneAndUpdate({ _id: this.req.body.orderId, userId: this.req.currentUser._id }, productDetails, { upsert: true });
                if (exportLib._.isEmpty(orderDetails)) {
                    return this.res.send({ status: 0, message: exportLib.i18n.__('NOT_FOUND') });
                }
                return this.res.send({ status: 1, message: exportLib.i18n.__('UPDATED_SUCCESSFULLY') });
            }
            else {
                return this.res.send({ status: 1, message: exportLib.i18n.__('REQUEST_PARAMETERS') });
            }
        } catch (error) {
            console.log("error", error)
            return this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') });
        }
    }

    /****** getting product details ******/
    async productDetails(data) {
        return new Promise(async (resolve, reject) => {
            try {
                let { productsArray } = data;
                let products = []; let count = 0; let totalAmount = 0;
                if (productsArray.length > 0) {
                    for (let i = 0; i < productsArray.length; i++) {
                        if (productsArray[i].quantity > 0) {
                            /**** Getting product details *****/
                            let productsDetails = await Products.findOne({ _id: productsArray[i].productId }, { __v: 0, createdAt: 0, updatedAt: 0 });
                            if (exportLib._.isEmpty(productsDetails)) { return resolve({ status: 0, message: exportLib.i18n.__('NOT_FOUND_DETAILS_OF_PRODUCT') }); }
                            await products.push({
                                productName: productsDetails.productName, productImage: productsDetails.productImage, SKU: productsDetails.SKU, price: productsDetails.price, quantity: productsArray[i].quantity, customUrl: productsDetails.customUrl
                            })
                            count += productsArray[i].quantity;
                            totalAmount += (productsDetails.price) * (productsArray[i].quantity);
                        }
                    }
                    return resolve({ products, count, totalAmount })
                } else {
                    return resolve({ products, count, totalAmount })
                }
            } catch (error) {
                return reject(error)
            }
        })

    }

    /********************************************************
   Purpose: products listing
   Method: Post
   Authorisation: false
   Parameter:
   {
       "page":1,
       "pagesize":10
   }
   Return: JSON String
   ********************************************************/
    async ordersListing() {
        let _this = this;
        try {
            let data = _this.req.body;
            /****** pagination details starts *******/
            let page = (data.page) ? parseInt(data.page) : 1;
            let pagesize = (data.pagesize) ? parseInt(data.pagesize) : 999;
            let skip = (page && pagesize) ? ((page - 1) * pagesize) : 0;
            let limit = pagesize ? pagesize : 20;
            let sort = data.sort ? data.sort : { _id: -1 };
            /****** pagination details ends *******/
            let andArray = [{ userId: this.req.currentUser._id }]
            /**** data getting based on search  of productName, description and orderStatus ****/
            if (this.req.body.searchText) {
                let products = { 'products.productName': { $regex: ".*" + this.req.body.searchText + ".*", $options: 'i' } }
                let description = { 'products.description': { $regex: ".*" + this.req.body.searchText + ".*", $options: 'i' } }
                let orderStatus = { 'orderStatus': { $regex: ".*" + this.req.body.searchText + ".*", $options: 'i' } }
                let orquery = { $or: [products, description, orderStatus] };
                andArray.push(orquery);
            }
            let orders = await Orders.find({ $and: andArray }, { orderId: 1, orderStatus: 1, count: 1, totalAmount: 1, createdAt: 1, products: 1 }).sort(sort).skip(skip).limit(limit);
            let total = await Orders.find({ $and: andArray }).countDocuments();
            /***** final output of the products *****/
            let resData = {
                currentPage: page,
                total,
                orders,
            };
            return _this.res.send({ status: 1, message: exportLib.i18n.__('DETAILS'), data: resData });
        } catch (error) {
            console.log("error", error);
            return _this.res.send({ status: 0, message: exportLib.i18n.__('INTERNAL_SERVER_ERROR') });
        }
    }

}
module.exports = OrderController
