module.exports = (app, express) => {

    const router = express.Router();
    const exportLib = require('../../services/Exports');
    const OrderController = require('./Controller');
    const Validators = require('./Validator');

    router.post('/checkOut', exportLib.Globals.isAuthorised, Validators.checkoutValidator(), Validators.validate, (req, res, next) => {
        const orderObj = (new OrderController()).boot(req, res);
        return orderObj.checkOut();
    });

    router.post('/cancelOrder', exportLib.Globals.isAuthorised, Validators.orderId(), Validators.validate, (req, res, next) => {
        const orderObj = (new OrderController()).boot(req, res);
        return orderObj.cancelOrder();
    });

    router.get('/getOrderDetails/:orderId', exportLib.Globals.isAuthorised, Validators.orderId(), Validators.validate, (req, res, next) => {
        const orderObj = (new OrderController()).boot(req, res);
        return orderObj.getOrderDetails();
    });

    router.post('/updateOrder', exportLib.Globals.isAuthorised, Validators.updateOrder(), Validators.validate, (req, res, next) => {
        const orderObj = (new OrderController()).boot(req, res);
        return orderObj.updateOrder();
    });

    router.post('/ordersListing', exportLib.Globals.isAuthorised, (req, res, next) => {
        const orderObj = (new OrderController()).boot(req, res);
        return orderObj.ordersListing();
    });


    app.use(exportLib.configs.baseApiUrl, router);
}