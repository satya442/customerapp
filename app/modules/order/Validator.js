/****************************
 Validators
 ****************************/
const { validationResult, check } = require('express-validator');
const exportLib = require('../../services/Exports')

class Validators {

    /********************************************************
     Purpose: Function for checkout validator
     Parameter:
     {}
     Return: JSON String
     ********************************************************/
    static checkoutValidator() {
        try {
            return [
                check('shippingAddress').exists().withMessage(exportLib.i18n.__("%s REQUIRED", 'shippingAddress')),
                ...this.products()
            ];
        } catch (error) {
            return error;
        }
    }

    /********************************************************
    Purpose: Function for orderId validator
    Parameter:
    {}
    Return: JSON String
    ********************************************************/
    static orderId() {
        try {
            return [
                check('orderId').exists().withMessage(exportLib.i18n.__("%s REQUIRED", 'orderId')),
            ];
        } catch (error) {
            return error;
        }
    }

    /********************************************************
  Purpose: Function for updateOrder validator
  Parameter:
  {}
  Return: JSON String
  ********************************************************/
    static updateOrder() {
        try {
            return [
                ...this.orderId(),
                ...this.products()
            ];
        } catch (error) {
            return error;
        }
    }


    /********************************************************
  Purpose: Function for products validator
  Parameter:
  {}
  Return: JSON String
  ********************************************************/
    static products() {
        try {
            return [
                check('products').exists().withMessage(exportLib.i18n.__("%s REQUIRED", 'products')),
            ];
        } catch (error) {
            return error;
        }
    }

    static validate(req, res, next) {
        try {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(422).json({ status: 0, message: errors.array() });
            }
            next();
        } catch (error) {
            return res.send({ status: 0, message: error });
        }
    }

}

module.exports = Validators;