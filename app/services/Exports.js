const ObjectId = require('mongodb').ObjectID;
const _ = require("lodash");
const i18n = require("i18n");
const Globals = require("../../configs/Globals");
const globalObj = new Globals();
const Form = require("./Form");
const formObj = new Form();
const File = require("./File");
const fileObj = new File();
const Common = require("./Common");
const commonObj = new Common();
const path = require('path');
const fs = require('fs');
const configs = require("../../configs/configs");

module.exports = {
    ObjectId,
    _,
    i18n,
    Globals,
    globalObj,
    formObj,
    fileObj,
    commonObj,
    path,
    fs,
    configs,
};