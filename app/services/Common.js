/****************************
Common services
****************************/
const bcrypt = require('bcrypt');

class Common {
    constructor() {
    }

    /********************************************************
      Purpose: Encrypt password
      Parameter:
      {
          "data":{
              "password" : "test123"
          }
      }
      Return: JSON String
      ********************************************************/
    ecryptPassword(data) {
        return new Promise(async (resolve, reject) => {
            try {
                if (data && data.password) {
                    let password = bcrypt.hashSync(data.password, 10);
                    return resolve(password);
                }
                return resolve();
            } catch (error) {
                reject(error);
            }
        });
    }

    /********************************************************
    Purpose: Compare password
    Parameter:
    {
        "data":{
            "password" : "Buffer data", // Encrypted password
            "savedPassword": "Buffer data" // Encrypted password
        }
    }
    Return: JSON String
    ********************************************************/
    verifyPassword(data) {
        return new Promise(async (resolve, reject) => {
            try {
                let isVerified = false;
                if (data && data.password && data.savedPassword) {
                    var base64data = Buffer.from(data.savedPassword, 'binary').toString();
                    isVerified = await bcrypt.compareSync(data.password, base64data)
                }
                return resolve(isVerified);
            } catch (error) {
                reject(error);
            }
        });
    }

}
module.exports = Common;
