/****************************
 FILE HANDLING OPERATIONS
 ****************************/
let fs = require('fs');
let path = require('path');
const mv = require('mv');


class File {

    constructor(file, location) {
        this.file = file;
        this.location = location;
    }

    saveImage(data) {
        return new Promise(async (resolve, reject) => {
            let { filePath, imageName } = data;
            let dirPath = path.join(__dirname, '../..', 'public', 'upload', imageName);
            mv(filePath, dirPath, { mkdirp: true }, (err, value) => {
                if (err) return reject(JSON.stringify({ code: 'INTERNAL_SERVER_ERROR', message: response[500] }));
                else resolve(true);
            })
        });
    }

    readFile(filepath) {
        return new Promise((resolve, reject) => {
            fs.readFile(filepath, 'utf-8', (err, html) => {
                if (err) { return reject({ message: err, status: 0 }); }

                return resolve(html);

            });
        });
    }

}

module.exports = File;