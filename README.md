# Node Project

## Prequisites (Development):

| Module | Version |
| --- | --- |
| Node | 12.16.0 |
| Npm | 6.13.4 |

##### Install node modules
> npm install

##### For Project running command
> Create configs.js file in configs folder and copy configSample.js file.
> node server.js

##### swaggerUrl
> http://localhost:4000/customer
> userName: lakshmi
> password: lakshmi

##### apiUrl
> http://localhost:4000/api

##### fileUrl
> http://localhost:4000/public/upload



